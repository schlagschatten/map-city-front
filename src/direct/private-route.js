import React from 'react';
import {Route, Redirect} from 'react-router-dom';
import useToken from "../components/useToken";

const PrivateRoute = ({component: Component, ...rest}) => {

    const {token} = useToken()

    return (

        <Route {...rest}
               render={props => {
                   if (token) {
                       return <Component {...props} />
                   } else {
                       return <Redirect to="/login"/>
                   }
               }
               }
        />
    );
};

export default PrivateRoute;