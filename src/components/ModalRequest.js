import React, {useEffect, useState} from 'react';
import Modal from "react-modal";
import axios from "axios";
import useToken from "./useToken";

const customStyles = {
    content: {
        width: '20%',
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
    }
};

const requestAboutProblem = async (cred) => {
    await axios.post('http://localhost:8000/requestAboutProblem', cred)
}

const ModalRequest = props => {

    const {getPersonId} = useToken();
    const {getPersonLogin} = useToken();

    const {modalIsOpen, modalIsClose, view, setModalIsOpen} = props;

    const [categoryId, setCategoryId] = useState(0)
    const [categories, setCategories] = useState([])
    const [titleProblem, setTitleProblem] = useState("")
    const [photo, setPhoto] = useState(null)
    const [pointOnMap, setPointOnMap] = useState("")


    const getCategoryProblem = async () => {

        try {
            const res = await axios.get('http://localhost:8000/category')
            return res.data;

        } catch (error) {
            throw new Error(`Unable to get category that use `);
        }
    };
    useEffect(() =>{
        getCategoryProblem()
            .then(res => {
                setCategories(res)
                setCategoryId(res[0].id)
            })
    }, [])

    const handleAddPoint = () => {
        setModalIsOpen(false)
        let handler = null;
        const grabPoint = (event) =>{
            let lat = Math.round(event.mapPoint.latitude * 1000) / 1000;
            let lon = Math.round(event.mapPoint.longitude * 1000) / 1000;


            setPointOnMap([lat,lon])
            handler.remove()

            setModalIsOpen(true)
        }

        handler = view.on("click", grabPoint)
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        const pointOnMapObject = {
            type: "Point",
            coordinates: pointOnMap
        }

        const toBase64 = file => new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        });

        const imageStringBase64 = await toBase64(photo)

        await requestAboutProblem({
            personId: +getPersonId(),
            categoryId: +categoryId,
            titleProblem,
            photo: imageStringBase64,
            repairsMade: false,
            pointOnMap: pointOnMapObject
        });

        modalIsClose()

        document.location.reload();
    }

    const photoHandlerChange = (event) => {
        const photo = event.currentTarget.files[0]
        setPhoto(photo)
    }

    return (
        <div>
                <div>

                    <Modal
                        isOpen={modalIsOpen}
                        onRequestClose={modalIsClose}
                        style={customStyles}
                        contentLabel="Example Modal"
                        appElement={document.getElementById('root')}
                    >
                        <form className="form-input-modal" onSubmit={handleSubmit}>


                            <div className="modal-item">
                                <label className="modal-title-input">Category</label>
                                <select id="filter" className="esri-select" onChange={e => setCategoryId(+e.target.value)}>
                                    {categories.map(category_one => {
                                        return  <option value={category_one.id}>{category_one.name}</option>
                                    })}
                                </select>
                            </div>

                            <div className="modal-item">
                                <label className="modal-title-input">Name problem</label>
                                <div>
                                    <input
                                        name="titleProblem"
                                        className="form-modal"
                                        placeholder="Title Problem"
                                        value={titleProblem}
                                        onChange={e => setTitleProblem(e.target.value)}
                                    />
                                </div>
                            </div>

                            <div className="modal-item">
                                <label className="modal-title-input">Image problem</label>
                                <div>
                                    <input
                                        name="photo"
                                        className="form-modal-image"
                                        type="file"
                                        onChange={e => photoHandlerChange(e)}
                                    />
                                </div>
                            </div>

                            <div className="modal-item">
                                <label className="modal-title-input">Point on map</label>
                                <div>
                                    <input
                                        name="pointOnMap"
                                        className="form-modal"
                                        placeholder="Point On Map"
                                        value={pointOnMap}
                                    />
                                </div>
                            </div>

                            <div>
                                <button className="btn add-point form-button" type="button" onClick={handleAddPoint}>
                                    Add Point
                                </button>
                                <div className="other-btn-container">
                                    <button className="btn other-form-button form-button" type="submit" onSubmit={handleSubmit}  disabled={pointOnMap=="" || photo==null}>
                                        Add
                                    </button>
                                    <button className="btn form-button" type="button" onClick={modalIsClose} >
                                        No Thanks
                                    </button>
                                </div>

                            </div>

                        </form>

                    </Modal>

                </div>
        </div>
    );
};

export default ModalRequest;