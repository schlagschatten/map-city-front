import React, {useEffect, useState} from 'react';
import axios from "axios";
import useToken from "./useToken";
import ProblemItem from "./ProblemItem";


const Profile = () => {

    const {getPersonId, getPersonLogin, deleteToken} = useToken();

    const [problems, setProblems] = useState([])

    const exitUser = () => {
        deleteToken()
        window.location.href = '/'
    }

    const getRequestAboutProblems = async () => {

        try {
            const res = await axios.get('http://localhost:8000/requestAboutProblem')
            return res.data;

        } catch (error) {
            throw new Error(`Unable to get category that use `);
        }
    };

    useEffect(() =>{
        getRequestAboutProblems()
            .then(res => setProblems(res.filter((problem) => getPersonId() === problem.person.id)))
    }, [])

    return (
        <div>
            <ul className="problem-container">
                <div className="problem-data-exit">
                    <li style={{fontSize: "24px", color: "#0079c1", fontWeight: 400}}>Hello {getPersonLogin()}</li>
                    <button className="btn exit-button" onClick={exitUser}>exit</button>
                </div>

            {problems.map(x=>{
                return <li><ProblemItem data={x}/></li>
            })}
            </ul>
        </div>
    );

};

export default Profile;