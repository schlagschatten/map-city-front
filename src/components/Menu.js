import React from 'react';
import '../App.css'

const Menu = ({header, items, active, setActive}) => {


    const checkKey = (item) => {
            window.location.href = item.href
    }

    return (
        <div className={active ? 'menu active' : 'menu'} onClick={() => setActive(false)}>
            <div className='blur'/>
            <div className='menu_content' onClick={e => e.stopPropagation()}>
                <div className='menu_header'>{header}</div>
                <ul>
                    {items.map(item =>
                        <li>
                            <a style={{cursor: "pointer"}} onClick={e => checkKey(item)}>{item.value}</a>
                        </li>
                    )}
                </ul>
            </div>
        </div>
    );
};

export default Menu;
