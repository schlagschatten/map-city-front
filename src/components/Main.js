import React from 'react';
import MapCity from "./MapCity";


const Main = () => {

    return (
        <div>
            <div className="main_page_container">
                <MapCity key={'map-city'}/>
            </div>
        </div>

    );
};

export default Main
