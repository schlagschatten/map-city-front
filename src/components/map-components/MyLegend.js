import  { useState, useEffect } from 'react';
import { loadModules } from 'esri-loader';

const MyLegend = (props) => {

    // console.log('MyLegend')

    const [myLegend, setMyLegend] = useState(null);

    useEffect(() => {
        loadModules(['esri/widgets/Legend', 'esri/widgets/Expand'])
            .then(([Legend, Expand]) => {
            const myLegendObj = new Legend({
                view: props.view,
                container: "legendDiv"
            });

            const infoDiv = document.getElementById("infoDiv");
            const uiItem =  new Expand({
                view: props.view,
                content: infoDiv,
                expandIconClass: "esri-icon-layer-list",
                expanded: true
            });

            console.log("uiItem", uiItem)

            setMyLegend(uiItem);
            props.view.ui.add(uiItem, "top-right");
            // props.map.add(myLegend);
        }).catch((err) => console.error(err));
        return function cleanup() {
            props.view.ui.remove(myLegend);
        }
    },[] );

    return null;
}

export default MyLegend;