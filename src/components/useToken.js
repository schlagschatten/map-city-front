import { useState } from 'react';

export default function useToken() {
    const getToken = () => {
        const tokenString = localStorage.getItem('token');
        const userToken = JSON.parse(tokenString);
        return userToken?.accessToken
    };

    const getPersonId = () => {
        const tokenString = localStorage.getItem('token');
        const userToken = JSON.parse(tokenString);
        return userToken?.personId
    }

    const getPersonLogin = () => {
        const tokenString = localStorage.getItem('token');
        const userToken = JSON.parse(tokenString);
        return userToken?.login
    }

    const [token, setToken] = useState(getToken());

    const saveToken = userToken => {
        localStorage.setItem('token', JSON.stringify(userToken));
        setToken(userToken);
    };

    const deleteToken = () => {
        localStorage.removeItem('token')
    }

    return {
        setToken: saveToken,
        token,
        getPersonId,
        getPersonLogin,
        deleteToken,
        getToken
    }
}