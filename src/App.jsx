import React, {useState} from 'react';
import './App.css';
import {BrowserRouter as Router, Route} from "react-router-dom";
import Main from "./components/Main";
import FormLogin from "./components/FormLogin"
import FormRegister from "./components/FormRegister";
import PrivateRoute from "./direct/private-route";
import Profile from "./components/Profile";
import Menu from "./components/Menu";

const App = () => {

    const [menuActive, setMenuActive] = useState(false)
    const items = [
        {value: "Main", href: '/'},
        {value: "Profile", href: '/profile'},
    ]

    return (
        <section className="App">
            <div className='app'>
                <nav>
                    <div className="main-menu-btn" onClick={() => setMenuActive(!menuActive)}>
                        <span></span>
                    </div>
                </nav>
                <main>
                    <div>
                        {<Menu active={menuActive} setActive={setMenuActive} header={"Menu"} items={items}/>}
                    </div>
                    <div>
                        <Router>
                            <Route exact path="/" component={Main} />
                            <PrivateRoute exact path="/profile" component={Profile} />
                            <Route exact path="/login" component={FormLogin} />
                            <Route exact path="/register" component={FormRegister} />
                            {/*<Route exact path="/auth" component={Profile}/>*/}
                        </Router>
                    </div>
                </main>
            </div>
        </section>
    );
};
export default App;
